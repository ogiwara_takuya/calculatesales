package jp.alhinc.ogiwara_takuya.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {

	public static void main(String[] args){
		//エラー1,2 コマンドライン引数がない場合・2個以上ある場合
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//(コード,支店)マップ
		HashMap<String, String> branchMap = new HashMap<String, String>();
		//(コード,金額)マップ
		HashMap<String, Long> branchMoneyMap = new HashMap<String, Long>();

		//合計金額を足す用
		long moneyTotal = 0 ;

		if(!inputFile(args[0], "branch.lst", branchMap, branchMoneyMap, "支店", "^[0-9]{3}")){
			return;
		}

		ArrayList<String> rcdList = new ArrayList<String>();
		BufferedReader bs = null;

		//フォルダ名を取得
		File files = new File(args[0]);
		String[] fileName = files.list();
		File[] fileNameFile = files.listFiles();

		//8桁.rcdのファイルのみのリスト
		for(int i = 0; i < fileName.length; i++){
			//条件設定「rcd」&&「8桁」
			if(fileNameFile[i].isFile() && fileName[i].matches("[0-9]{8}.rcd")){
				rcdList.add(fileName[i]);
			}
		}


		//エラー14~18 連番チェック
		for(int i = 0; i < (rcdList.size()-1); i++){
			String[] rcdListSpt = rcdList.get(i).split("\\.");
			String[] rcdListSptAfter = rcdList.get(i + 1).split("\\.");
			int numberBefore = Integer.parseInt(rcdListSpt[0]);
			int numberAfter = Integer.parseInt(rcdListSptAfter[0]) - 1;

			if(numberBefore != numberAfter){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//抽出されたファイルの読み込み
		for(int i = 0; i < rcdList.size(); i++){
			try{
				File rcdFile = new File(args[0], rcdList.get(i));
				FileReader fs = new FileReader(rcdFile);
				bs = new BufferedReader(fs);
				String rcdLine;
				ArrayList<String> rcdArrayList = new ArrayList<String>();

				//1行ずつ読み込んでる
				while((rcdLine = bs.readLine()) != null){
					rcdArrayList.add(rcdLine);
				}
				//エラー25,26 2行以外の場合
				if(rcdArrayList.size() != 2){
					System.out.println(fileName[i] + "のフォーマットが不正です");
					return;
				}

				//エラー19~22 数字以外があった場合
				if(!rcdArrayList.get(1).matches("^[0-9]*$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//1行目をearningNumberで定義
				String rcdNumber = rcdArrayList.get(0);

				//エラー24 支店コードが存在しない場合
				if(branchMap.containsKey(rcdNumber) != true){
					System.out.println(fileName[i] + "の支店コードが不正です");
					return;
				}

				//マップから取り出し足す
				moneyTotal = branchMoneyMap.get(rcdNumber) + Long.parseLong(rcdArrayList.get(1));
				branchMoneyMap.put(rcdNumber, moneyTotal);

				////エラー23 合計金額が10桁を超えたとき
				long digit = 9999999999L;
				if(moneyTotal > digit){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;

			}finally{
				try{
					if(bs != null){
						bs.close();
					}
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		if(!outputFile(args[0], "branch.out", branchMap, branchMoneyMap)){
			return;
		}
	}

	//ファイルの読み込み
	public static boolean inputFile(String filePath, String inputFileName, HashMap<String,String> nameMap, HashMap<String,Long> moneyMap, String name, String code){
		//合計金額を足す用
		long moneyTotal = 0 ;
		//finally用
		BufferedReader br = null;
		try{
			File listFile = new File(filePath, inputFileName);
			//エラー3 branch.lstがなかったとき
			if(!listFile.exists()){
				System.out.println(name + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(listFile);
			br = new BufferedReader(fr);
			String fileList;

			//1行ずつ抽出
			while((fileList = br.readLine()) != null){
				//「,」で区切る
				String[] address = fileList.split(",", -1);

				//エラー4~11 支店コード桁数が3桁以外
				if(!address[0].matches(code)){
					System.out.println(name + "定義ファイルのフォーマットが不正です");
					return false;
				}
				//エラー12,13 支店ファイル要素数2つ以外の場合
				if(address.length != 2){
					System.out.println(name + "定義ファイルのフォーマットが不正です");
					return false;
				}
				//(支店コード,支店名)
				nameMap.put(address[0], address[1]);
				//(支店コード,合計金額(0))
				moneyMap.put(address[0], moneyTotal);
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			try{
				if(br != null){
					br.close();
				}
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return false ;
			}
		}
		return true;
	}


	//ファイル出力
	public static boolean outputFile(String filePath, String outputFileName, HashMap<String,String> nameMap, HashMap<String,Long> moneyMap){

		PrintWriter pw = null ;
		try{
			File outputFileData = new File(filePath, outputFileName);
			FileWriter fw = new FileWriter(outputFileData);
			BufferedWriter bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

			for(String number : nameMap.keySet()){
				 pw.println(number + "," + nameMap.get(number) + "," + moneyMap.get(number));
			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			try{
				if(pw != null){
					pw.close();
				}
			}catch(Exception e){
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}


